# Teža možganov pri sesalcih

Podatki: [mozgani.csv](mozgani.csv)

## Opis

Meritve telesne teže in teže možganov na vzorcu 59 vrst sesalcev.

## Format

Baza podatkov s 59 vrednostmi 4 spremenljivk

* *vrsta* je nominalna spremenljivka, katere vrednosti so latinski nazivi vrste sesalcev.
* *slovime* je nominalna spremenljivka, katere vrednosti so slovenski nazivi vrste sesalcev.
* *telteza* je numerična zvezna spremenljivka, ki predstavlja telesno težo (v kilogramih).
* *mozteza* je numerična zvezna spremenljivka, ki predstavlja težo možganov (v gramih).

## Raziskovalna domneva

Med težo možganov in telesno težo sesalcev obstaja funkcijska zveza.

## Opomba

Konstruirati linearni regresijski model med transformiranima spremenljivkama `log(mozteza)`
in `log(telteza)`.