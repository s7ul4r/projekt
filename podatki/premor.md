# Stevilo napak

Podatki: [premor.csv](premor.csv)

## Opis

Meritve števila napak na naslednjih vzorcih javnih uslužbencev

1. vzorec 23 uslužbencev, ki dobili dodatni 15-minutni premor tekom delovnega dneva in
2. vzorec 22 uslužbencev, ki niso dobili dodatnega premora tekom delovnega dneva.

## Format

Baza podatkov s 45 meritvami dveh spremenljivk

* *dodpremor* je nominalna spremenljivka, ki ima dve vrednosti: Da=ima dodatni premor,
Ne=nima dodatnega premora.
* *napake* je numerična diskretna spremenljivka, ki predstavlja število narejenih napak
tekom delovnega dneva.

## Raziskovalna domneva

Dodatni premor tekom dneva vpliva na zmanjšanje napak, ki naredijo javni uslužbenci.