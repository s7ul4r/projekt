# Vitalna kapaciteta

Podatki: [vitkap:csv](vitkap:csv)

## Opis

Meritve vitalne kapacitete (kapacitete pljuč) na naslednjih vzorcih moških delavcev, ki delajo
več kot 10 let v dveh tovarnah majhnega irskega mesta

1. vzorec 24 delavcev, ki delajo v industriji kadmija in
2. vzorec 22 delavcev, ki ne delajo v industriji kadmija.

## Format

Baza podatkov s 46 meritvami dveh spremenljivk

* *skupina* je nominalna spremenljivka (faktor), ki ima vrednosti Cd in Ne, ki opisuje pri-
padnost skupini.
* *vit.kapaciteta* je numerična zvezna spremenljivka, ki predstavlja vitalno kapaciteto (v
litrih).

## Raziskovalna domneva

Dolgotrajno delo v industriji kadmija vpliva na zmanjšanje kapacitete pljuč.