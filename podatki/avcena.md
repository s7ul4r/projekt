# Prodajna cena avtomobila

Podatki: [avcena.csv](avcena.csv)

## Opis

Meritve nabavne in prodajne cene na vzorcu 57 modelov avtomobilov.

## Format

Baza podatkov s 57 vrednostmi treh spremenljivk

* *model* je nominalna spremenljivka, katere vrednosti so nazivi modela avtomobila.
* *nabcena* je numerićna zvezna spremenljivka, ki predstavlja nabavno ceno avtomobila (v
1000 ameriških dolarjev).
* *prodcena* je numerična zvezna spremenljivka, ki predstavlja prodajno ceno avtomobila (v
1000 ameriških dolarjev).

## Raziskovalna domneva

Med prodajno in nabavno ceno avtomobila obstaja linearna funkcijska zveza.